// ==UserScript==
// @name           RED: Purchase Links for Music Requests
// @description    Find external streaming or purchase links (with prices, if possible) for 16-bit WEB requests.
// @author         _
// @version        0.92
// @grant          GM.xmlHttpRequest
// @connect        www.junodownload.com
// @connect        api.deezer.com
// @connect        bandcamp.com
// @connect        beatport.com
// @connect        bleep.com
// @connect        qobuz.com
// @connect        google.com
// @match          https://redacted.sh/requests.php?action=view&id=*
// @run-at         document-end
// @namespace      _
// ==/UserScript==

(() => {
  "use strict";

  if (!document.querySelector("div.header > h2 > a").nextSibling.textContent.match(/Music/)) return; // we only care about music
  const linkbox = document.querySelector("div.header > .linkbox");
  const request_id = new URL(window.location).searchParams.get("id");
  const promises = [];

  const decodeHTML = orig => {
    const txt = document.createElement("textarea");
    txt.innerHTML = orig;
    return txt.value;
  };

  const sanitize = text => {
    return text
      .replace(/\s+\(?EP\)?$/i, "")
      .replace(/\s+\(cover\)$/i, "")
      .toLowerCase()
      .trim();
  };

  const redAPI = async request_id => {
    try {
      const res = await fetch(
        `${location.origin}/ajax.php?action=request&id=${request_id}`
      );
      return res.json();
    } catch (error) {
      console.log(error);
    }
  };

  const corsFetch = url => {
    return new Promise((resolve, reject) => {
      GM.xmlHttpRequest({
        method: "GET",
        url: encodeURI(url),
        headers: {
          Origin: "no.origin.com"
        },
        onload: res => resolve(res.responseText),
        onerror: res => reject(res)
      });
    });
  };

  const fetchJSON = async url => {
    const responseText = await corsFetch(url);
    return JSON.parse(responseText);
  };

  const fetchDOM = async url => {
    const responseText = await corsFetch(url);
    const parser = new DOMParser();
    return parser.parseFromString(responseText, "text/html");
  };

  const deezSearch = async (artist, album) => {
    try {
      const response = await fetchJSON(
        `https://api.deezer.com/search?q=artist:"${artist}" album:"${sanitize(album)}"`
      );
      const deezer_id = response.data[0].album.id;
      const deezer_api = await fetchJSON(
        `https://api.deezer.com/album/${deezer_id}`
      );
      let deezer_release_date = deezer_api.release_date;
      const deezerDate = new Date(deezer_release_date);
      if (Date.now() - deezerDate > 0) {
        deezer_release_date = "";
      } else {
        deezer_release_date = ` (<strong class="important_text">${deezer_release_date}</strong>)`;
      }
      linkbox.insertAdjacentHTML(
        "afterbegin",
        `<a href=https://www.deezer.com/album/${deezer_id} target="_blank" class="brackets">Deezer${deezer_release_date}</a>`
      );
    } catch (e) {
      console.error(
        `Oops! Deezer search failed (probably, release not found)...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  const qobuzSearch = async (artist, album) => {
    try {
      const query = `${artist} - ${album}`;
      const doc = await fetchDOM(
        `https://qobuz.com/gb-en/search?q=${query}`
      );
      const releases = [...doc.querySelectorAll(".ReleaseCard")];
      const found_album = releases.find(release => {
        if (sanitize(release.querySelector('.ReleaseCardInfosSubtitle > a').innerText).includes(sanitize(artist))) {
          return sanitize(release.querySelector('.ReleaseCardInfosTitle').dataset.title).includes(sanitize(album));
        }
      });
      const purchaseLink = found_album.querySelector('.ReleaseCardInfosTitle').href;
      let purchasePrice = "";
      try {
        purchasePrice = ` (<u>${found_album.querySelector('div.price-box > div.price > div.quality-smr > span:nth-child(2)').innerText.trim()}</u>)`;
      } catch (e) {
        console.error(
          `Oops! Could not capture Qobuz album price...${e && e.message && `\n(${e.message})`}`
        );
      }
      linkbox.insertAdjacentHTML(
        "afterbegin",
        `<a href=${purchaseLink.replace("https://redacted.ch", "https://www.qobuz.com")} target="_blank" class="brackets">Qobuz${purchasePrice}</a>`
      );
    } catch (e) {
      console.error(`Oops! Qobuz search failed (probably, release not found)...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  // bleep's internal search is cumbersome; better luck using google
  const bleepSearch = async (artist, album) => {
    try {
      const doc = await fetchDOM(
        `https://google.com/search?q=${artist} - ${album} site:bleep.com`
      );
      const nodes = [...doc.querySelectorAll(".LC20lb")];
      const found_album = nodes.find(
        item =>
          sanitize(item.innerText).includes(sanitize(album)) &&
          sanitize(item.innerText).includes(sanitize(artist)))
      const bleepLink = found_album.parentElement.href;
      insertBleepLink(bleepLink);
    } catch (e) {
      console.error(
        `Oops! Bleep search failed (probably, release not found)...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  const insertBleepLink = async bleepLink => {
    try {
      const doc = await fetchDOM(bleepLink);
      let purchasePrice = "";
      try {
        purchasePrice = ` (<u>${doc.querySelectorAll("li#format-download .price-wrapper")[1].innerText.trim()}</u>)`;
      } catch (e) {
        console.error("Couldn't find a price for Bleep album.");
      }
      linkbox.insertAdjacentHTML(
        "afterbegin",
        `<a href=${bleepLink} target="_blank" class="brackets">Bleep${purchasePrice}</a>`
      );
    } catch (e) {
      console.error(
        `Oops! Could not insert Bleep link...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  // bandcamp's internal search is cumbersome; better luck using google
  const bandSearch = async (artist, album, label) => {
    try {
      const doc = await fetchDOM(
        `https://google.com/search?q=${label} ${artist} - ${album} site:bandcamp.com`
      );
      const nodes = [...doc.querySelectorAll(".LC20lb")];
      const found_album = nodes.find(
        item =>
          sanitize(item.innerText).includes(sanitize(album)) &&
          (sanitize(item.innerText).includes(sanitize(artist)) ||
          sanitize(item.innerText).includes(sanitize(label))));
      const bclink = found_album.parentElement.href;
      insertBCLink(bclink);
    } catch (e) {
      console.error(
        `Oops! Bandcamp search failed (probably, release not found)...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  const insertBCLink = async bclink => {
    try {
      const doc = await fetchDOM(bclink);
      const digital = [...doc.querySelectorAll("div.ft")].find(x =>
        x.innerText.includes("Digital")
      );
      let purchasePrice = "";
      try {
        purchasePrice = ` (<u>${digital
          .querySelector("span.nobreak > span.base-text-color")
          .innerText.trim()}</u>)`;
      } catch (e) {
        console.error("Couldn't find a price for Bandcamp album.");
      }
      linkbox.insertAdjacentHTML(
        "afterbegin",
        `<a href=${bclink} target="_blank" class="brackets">Bandcamp${purchasePrice}</a>`
      );
    } catch (e) {
      console.error(
        `Oops! Could not insert Bandcamp link...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  const beatSearch = async (artist, album) => {
    try {
      const query = `${artist} - ${album}`;
      const doc = await fetchDOM(
        `https://www.beatport.com/search?q=${query}`
      );
      const releases = [...doc.querySelectorAll('ul.bucket-items > li.bucket-item.ec-item.release')];
      const found_album = releases.find(release => {
        if (sanitize(release.querySelector('p.release-artists').innerText).includes(sanitize(artist))) {
          return sanitize(release.querySelector('p.release-title').innerText).includes(sanitize(album));
        }
      });
      const purchaseLink = found_album.querySelector('p.release-title > a').href;
      let purchasePrice = "";
      try {
        purchasePrice = ` (<u>${found_album.querySelector('div.buy-button').dataset.price.trim()}</u>)`;
      } catch (e) {
        console.error(
          `Oops! Could not capture Beatport album price...${e && e.message && `\n(${e.message})`}`
        );
      }
      linkbox.insertAdjacentHTML(
        "afterbegin",
        `<a href=${purchaseLink.replace("https://redacted.ch", "https://beatport.com")} target="_blank" class="brackets">Beatport${purchasePrice}</a>`
      );
    } catch (e) {
      console.error(`Oops! Beatport search failed (probably, release not found)...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  const junoSearch = async (artist, album) => {
    try {
      const query = `${artist} - ${album}`;
      const doc = await fetchDOM(
        `https://www.junodownload.com/search/?solrorder=relevancy&q[all][]=${query}&track_sale_format=flac`
      );
      const artists = [...doc.querySelectorAll(".juno-artist")];
      const found_artists = artists.filter(node =>
        sanitize(node.innerText).includes(sanitize(artist))
      );
      const found_album = found_artists.find(
        artist =>
          sanitize(artist.parentNode.nextElementSibling.querySelector(".juno-title").innerText) === sanitize(album)
      );
      const parentNode = found_album.parentNode.parentNode;
      const purchaseLink = "https://" + parentNode
        .querySelector(".juno-title")
        .href.replace(`${location.origin}`, "junodownload.com");
      let purchasePrice = "";
      try {
        purchasePrice = ` (<u>${parentNode
          .querySelector(".col.pl-2")
          .innerText.match(/\D\d+\.\d+\s?$/)[0]
          .trim()}</u>)`;
      } catch (e) {
        console.error(
          `Oops! Could not capture Juno album price...${e && e.message && `\n(${e.message})`}`
        );
      }
      linkbox.insertAdjacentHTML(
        "afterbegin",
        `<a href=${purchaseLink} target="_blank" class="brackets">Juno${purchasePrice}</a>`
      );
    } catch (e) {
      console.error(`Oops! Juno search failed (probably, release not found)...${e && e.message && `\n(${e.message})`}`
      );
    }
  };

  redAPI(request_id).then(({ response, status }) => {
    if (status !== "success") {
      console.log("API request failed; aborting.");
      return;
    }
    if (!response.mediaList.includes("WEB") && response.mediaList != "Any") {
      console.error("WEB cannot fill this request.");
      return;
    }
    if (!response.bitrateList.some(bitrate => /^Lossless$/.test(bitrate)) && response.bitrateList != "Any") {
      console.error("16-bit cannot fill this request.");
      return;
    }

    const rowRef = document.querySelector('div.main_column table.layout tbody tr');
    rowRef.insertAdjacentHTML(
      "beforebegin",
      '<td class="label">Purchase Links</td><td><span id="searching">Searching ...</span></td>'
    );
    const artist = decodeHTML(response.musicInfo.artists[0].name);
    const album = decodeHTML(response.title);
    const label = decodeHTML(response.recordLabel);
    const parser = new DOMParser();
    const description = parser.parseFromString(response.description, "text/html");
    const bandLink = [...description.getElementsByTagName("a")].find(anchor =>
      anchor.href.match(/\bhttps?:\/\/.+bandcamp.com/)
    );
    const bleepLink = [...description.getElementsByTagName("a")].find(anchor =>
      anchor.href.match(/\bhttps?:\/\/.+bleep.com/)
    );
    const deezLink = [...description.getElementsByTagName("a")].find(anchor =>
      anchor.href.match(/\bhttps?:\/\/.+deezer.com/)
    );
    if (deezLink == undefined) {
      promises.push(deezSearch(artist, album));
    } else {
      linkbox.insertAdjacentHTML(
        "afterbegin",
        `<a href=${deezLink.href} target="_blank" class="brackets">Deezer</a>`
      );
    }
    if (bandLink == undefined) {
      promises.push(bandSearch(artist, album, label));
    } else {
      promises.push(insertBCLink(bandLink.href));
    }
    if (bleepLink == undefined) {
      promises.push(bleepSearch(artist, album, label));
    } else {
      promises.push(insertBleepLink(bleepLink.href));
    }
    promises.push(junoSearch(artist, album));
    promises.push(beatSearch(artist, album));
    promises.push(qobuzSearch(artist,album));

    Promise.all(promises).then(() => {
      document.getElementById("searching").innerHTML = "Searching ... <strong><u>Done!</u></strong>";
    });
  });

})();